// Fill the DB with example data on startup

import { Meteor } from 'meteor/meteor';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import { Fields } from '../../api/fields/fields.js';
import {DocumentDefinitions} from "../../api/document-definitions/document-definitions";
import {Layouts} from "../../api/layouts/layouts";


Meteor.startup(() => {
    // if the Links collection is empty

    console.log("Fixtures: clear all collections");
    resetDatabase();

    // Insert fields
    const fields = [
        {
            label: "Client Name",
            name: "name",
            type: "text",
            maxLength: 100
        },
        {
            label: "Client Age",
            name: "age",
            type: "number"
        }
    ];

    _.each(fields, (field) => Fields.insert(field, () => console.log('Fixtures: Field inserted.')));

    const fieldName = Fields.findOne({name: "name"});
    const fieldAge = Fields.findOne({name: "age"});

    console.log(`Field Name ID: ${fieldName._id}`, `\nField Age ID: ${fieldAge._id}`);

    // Insert documentdefinitions
    const docDefinitionId = DocumentDefinitions.insert({
        schema: {
            fields: [fieldName, fieldAge]
        }
    }, () => console.log('Fixtures: DocumentDefinitions inserted'));


    Layouts.insert({
        name: "documentPageLayout",
        label: "Document Page Layout",
        documentDefinitionId: docDefinitionId,
        header: {
            rows: [
                {
                    columns: [
                        {
                            fieldId: fieldName._id,
                        },
                        {
                            fieldId: fieldAge._id,
                        }
                    ]
                }
            ]
        },
        buttons: ["Submit"]
    }, () => console.log('Fixtures: Layouts inserted'));


});
