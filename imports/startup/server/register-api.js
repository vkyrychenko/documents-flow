// Register your apis here

// Fields API
import '../../api/fields/methods.js';
import '../../api/fields/server/publications.js';

// DocumentDefinitions API
import '../../api/document-definitions/methods.js';
import '../../api/document-definitions/server/publications.js';

// Layouts API
import '../../api/layouts/methods.js';
import '../../api/layouts/server/publications.js';

// Documents API
import '../../api/documents/methods.js';
import '../../api/documents/server/publications.js';
