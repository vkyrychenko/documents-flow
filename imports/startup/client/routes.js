import {FlowRouter} from 'meteor/kadira:flow-router';
import {BlazeLayout} from 'meteor/kadira:blaze-layout';
import './register-templates';


// Set up all routes in the app
FlowRouter.route('/', {
    name: 'App.home',
    action() {
        BlazeLayout.render('App_body', {main: 'App_home'});
    },
});

FlowRouter.route('/fields', {
    name: 'Fields.list',
    action() {
        BlazeLayout.render('App_body', {main: 'Fields_list'});
    },
});

FlowRouter.route('/definitions', {
    name: 'DocumentDefinitions.list',
    action() {
        BlazeLayout.render('App_body', {main: 'DocumentDefinitions_list'});
    },
});

FlowRouter.route('/documents', {
    name: 'Documents.list',
    action() {
        BlazeLayout.render('App_body', {main: 'Documents_list'});
    },
});

FlowRouter.route('/documents/definition/:definitionId/layout/:layoutId/create', {
    name: 'Documents.fromLayout.create',
    action() {
        BlazeLayout.render('App_body', {main: 'Documents_create'});
    },
});


FlowRouter.notFound = {
    action() {
        BlazeLayout.render('App_body', {main: 'App_notFound'});
    },
};
