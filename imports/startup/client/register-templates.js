// Import needed templates
import '../../ui/layouts/body/body.js';
import '../../ui/pages/home/home.js';
import '../../ui/pages/not-found/not-found.js';

import '../../ui/pages/fields/fields-list';
import '../../ui/pages/document-definitions/document-definitions-list';
import '../../ui/pages/documents/documents-create';
import '../../ui/pages/documents/documents-list';