import './documents-create.html';
import './documents-list.html';
import '../not-found/not-found';
import { Template } from 'meteor/templating';
import {Documents} from "../../../api/documents/documents";
import {Views, ViewsHelper} from "../../../api/views/client/views";

/**
 * @namespace Client.Templates.DocumentDefinitions
 * @memberof Client.Templates
 */

/**
 * DocumentDefinitions: Event Handlers
 * @memberof Client.Templates.DocumentDefinitions
 * @member Events
 */
Template.Documents_create.events({
    'submit .new-document'(event) {
        // Prevent default browser form submit
        event.preventDefault();

        let helper = Template.Documents_create.viewsHelper;

        const formData = $(event.target).find("input.js-field-data");//$(event.target).serializeArray();
        const fieldsData = [];

        formData.each((i, el) => {
            let fieldId = $(el).attr('id');

            if(!fieldId){
                console.log('There is no fieldId attribute'); return;
            }

            let field = helper.getDefinitionFieldById(fieldId);

            if(!field){
                console.log(`Can not find field by ID: ${fieldId}`); return;
            }

            field.value = $(el).val();
            fieldsData.push(field);
        });

        let docName = $(event.target).find("input[name=documentName]").val();
        let docLabel = $(event.target).find("input[name=documentLabel]").val();

        if(fieldsData && fieldsData.length > 0 && docName && docLabel) {

            Documents.insert({
                name: docName,
                label: docLabel,
                fields: fieldsData
            });
        }

        // Clear form
        $(event.target).find("input, textarea").val("");

        $('.js-alert').removeClass('hidden');
        setTimeout(() => { $('.js-alert').addClass('hidden'); }, 8000);
    },
});

/**
 * DocumentDefinitions: Helpers
 * @memberof Client.Templates.DocumentDefinitions
 * @member Helpers
 */
Template.Documents_create.helpers({
    docLayout(){
        return Views.findOne({documentDefinitionId: FlowRouter.getParam('definitionId')});
    },
    documents(){
        return Documents.find({}, {sort: {_id: -1}});
    }
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onCreated
 */
Template.Documents_create.onCreated(function() {
    Meteor.subscribe('documents.all');
    Template.Documents_create.initViewsHelper();
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onRendered
 */
Template.Documents_create.onRendered(function() {

});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onDestroyed
 */
Template.Documents_create.onDestroyed(function() {
});

Template.Documents_create.initViewsHelper = () => {
    const layoutId = FlowRouter.getParam('layoutId');
    const definitionId = FlowRouter.getParam('definitionId');

    console.log(`Layout: ${layoutId}`, `Definition: ${definitionId}`);

    Template.Documents_create.viewsHelper = new ViewsHelper(definitionId, layoutId);
    Template.Documents_create.viewsHelper.subscribe();
};

Template.Documents_create.viewsHelper = null;