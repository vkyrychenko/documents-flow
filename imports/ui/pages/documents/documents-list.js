import './documents-list.html';
import '../not-found/not-found';
import { Template } from 'meteor/templating';
import {Documents} from "../../../api/documents/documents";

/**
 * @namespace Client.Templates.DocumentDefinitions
 * @memberof Client.Templates
 */

/**
 * DocumentDefinitions: Event Handlers
 * @memberof Client.Templates.DocumentDefinitions
 * @member Events
 */
Template.Documents_list.events({

});

/**
 * DocumentDefinitions: Helpers
 * @memberof Client.Templates.DocumentDefinitions
 * @member Helpers
 */
Template.Documents_list.helpers({
    documents(){
        return Documents.find();
    }
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onCreated
 */
Template.Documents_list.onCreated(function() {
    Meteor.subscribe('documents.all');
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onRendered
 */
Template.Documents_list.onRendered(function() {

});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onDestroyed
 */
Template.Documents_list.onDestroyed(function() {
});