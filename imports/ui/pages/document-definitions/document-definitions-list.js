import { Template } from 'meteor/templating';
import {DocumentDefinitions} from "../../../api/document-definitions/document-definitions";
import './document-definitions-list.html';
import '../not-found/not-found';
import {Layouts} from "../../../api/layouts/layouts";

/**
 * @namespace Client.Templates.DocumentDefinitions
 * @memberof Client.Templates
 */

/**
 * DocumentDefinitions: Event Handlers
 * @memberof Client.Templates.DocumentDefinitions
 * @member Events
 */
Template.DocumentDefinitions_list.events({
});

/**
 * DocumentDefinitions: Helpers
 * @memberof Client.Templates.DocumentDefinitions
 * @member Helpers
 */
Template.DocumentDefinitions_list.helpers({
    definitions(){
        return DocumentDefinitions.find();
    },
    layoutsOfDefinition(definitionId){
        return Layouts.find({documentDefinitionId: definitionId});
    },
    rowNumber(index){
        return ++index;
    }
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onCreated
 */
Template.DocumentDefinitions_list.onCreated(function() {
    Meteor.subscribe('document-definitions.all');
    Meteor.subscribe('layouts.all');
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onRendered
 */
Template.DocumentDefinitions_list.onRendered(function() {
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onDestroyed
 */
Template.DocumentDefinitions_list.onDestroyed(function() {
});