import './fields-list.html';
import '../not-found/not-found';
// import { quickForm } from 'meteor/aldeed:autoforms';
import { Template } from 'meteor/templating';
import { Fields } from "../../../api/fields/fields";

/**
 * @namespace Client.Templates.DocumentDefinitions
 * @memberof Client.Templates
 */

/**
 * DocumentDefinitions: Event Handlers
 * @memberof Client.Templates.DocumentDefinitions
 * @member Events
 */
Template.Fields_list.events({
});

/**
 * DocumentDefinitions: Helpers
 * @memberof Client.Templates.DocumentDefinitions
 * @member Helpers
 */
Template.Fields_list.helpers({
    fields(){
        return Fields.find();
    },
    rowNumber(index){
        return ++index;
    }
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onCreated
 */
Template.Fields_list.onCreated(function() {
    Meteor.subscribe('fields.all');
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onRendered
 */
Template.Fields_list.onRendered(function() {
});

/**
 * @memberof Client.Templates.DocumentDefinitions
 * @member onDestroyed
 */
Template.Fields_list.onDestroyed(function() {
});