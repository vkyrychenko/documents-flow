import { Mongo } from 'meteor/mongo';
import {Layouts} from "../../layouts/layouts";
import {DocumentDefinitions} from "../../document-definitions/document-definitions";

/**
 * Local (client side only) collection to hold views.
 * @type {Mongo.Collection}
 */
const Views = new Mongo.Collection(null);

class ViewsHelper {

    constructor(definitionId, layoutId) {
        this._isReady = false;
        this._isLayoutsReady = false;
        this._isDefinitionsReady = false;

        this._definitionId = definitionId;
        this._layoutId = layoutId;

        this.collection = Views;
    }

    subscribe() {

        Meteor.subscribe('layouts.all', {
            onReady: () => {
                console.log('layouts.all - onReady');
                this._isLayoutsReady = true;
                this._setIsReady()
            }
        });

        Meteor.subscribe('document-definitions.all', {
            onReady: () => {
                console.log('document-definitions.all - onReady');
                this._isDefinitionsReady = true;
                this._setIsReady()
            }
        });
    }

    collection(){
        return this.collection();
    }

    isReady(){
        return this._isReady;
    }

    _generateViews() {
        if(!this._isReady){
            console.log("Subscription is not ready!");
            return false;
        }

        // const layout = Layouts.findOne({_id: layoutId});
        // const definition = DocumentDefinitions.findOne({_id: definitionId});
        const fields = this._getDefinitionFields();

        if(!fields || fields.length <= 0){
            console.log("There are no fields");
            return;
        }

        console.log("Fields:");
        console.log(fields);
        this._createViewFromLayout(fields);

    }

    _setIsReady(){
        this._isReady = (this._isDefinitionsReady && this._isLayoutsReady);

        if(this._isReady){
            this._onReady();
        }
    }

    _onReady(){
        this._generateViews();
    }

    _createViewFromLayout(definitionFields){
        const layout = Layouts.findOne({_id: this._layoutId});

        if(!layout){
            console.log(`There is no layout for ID: ${this._layoutId}`);
            return null;
        }

        let columns = [];
        layout.header.rows.forEach((row) => {
            row.columns = row.columns.map((col) => {
                let field = this._getFieldById(col.fieldId, definitionFields);

                if(field){
                    return field;
                }
            });
        });

        console.log("layout");
        console.log(layout);

        this.collection.insert(layout);
    }

    _getFieldById(id, fields){
        let field = null;

        fields.forEach((f) => {
            if(id === f._id){
                field = f;
            }
        });

        return field;
    }

    _getDefinitionFields(){
        const definition = DocumentDefinitions.findOne({_id: this._definitionId});

        if(!definition){
            console.log(`There is no definition for ID: ${this._definitionId}`);
            return null;
        }

        return definition.schema.fields;
    }

    getDefinitionFieldById(id){
        const fields = this._getDefinitionFields();

        if(!fields || fields.length <= 0){
            console.log("getDefinitionFieldById: There are no fields");
            return null;
        }

        return this._getFieldById(id, fields);
    }
}

export { Views, ViewsHelper };
