// Tests for the behavior of the links collection
//
// https://guide.meteor.com/testing.html

import { Meteor } from 'meteor/meteor';
import { assert } from 'chai';
import { Fields } from './fields.js';

if (Meteor.isServer) {
    describe('fields collection', function () {
        beforeEach(function () {
            console.log('fields: clearing collection');
            Fields.remove({});
        });
        it('insert correctly', function () {
            const fieldId = Fields.insert({
                label: "Client Name",
                name: "name",
                type: "text",
                maxLength: 100
            });
            const added = Fields.find({ _id: fieldId });
            const collectionName = added._getCollectionName();
            const count = added.count();

            assert.equal(collectionName, 'fields');
            assert.equal(count, 1);
        });
    });
}
