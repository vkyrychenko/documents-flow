import { Mongo } from 'meteor/mongo';
import { FieldsSchema } from './schemas/FieldsSchema';

const Fields = new Mongo.Collection('fields');

Fields.attachSchema(FieldsSchema);

Fields.allow({
    insert(field){
        return true;
    }
});

export { Fields };
