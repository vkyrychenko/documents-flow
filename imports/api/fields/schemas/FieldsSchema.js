import SimpleSchema from 'simpl-schema';

export const FieldsSchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true
    },
    label: {
        type: String,
        label: "Label"
    },
    name: {
        type: String,
        label: "Name"
    },
    type: {
        type: String,
        label: "Type"
    },
    maxLength: {
        type: Number,
        label: "Max Length",
        optional: true
    },
});