import { Meteor } from 'meteor/meteor';
import { Fields } from './fields.js';

Meteor.methods({
    'fields.insert'(field) {

        const validationContext = Fields.simpleSchema(field).newContext();
        const isValid = validationContext.isValid();

        console.log(`fields.insert - isValid: ${isValid}`);

        if(!isValid){
            return null;
        }
        return Fields.insert(field);
    },
});
