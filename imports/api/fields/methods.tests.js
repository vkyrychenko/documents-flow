// Tests for fields methods

import { Meteor } from 'meteor/meteor';
import { assert } from 'chai';
import { Fields } from './fields.js';
import './methods.js';

if (Meteor.isServer) {
  describe('fields methods', function () {
    beforeEach(function () {
        Fields.remove({});
    });

    it('can add a new field', function () {
      const addField = Meteor.server.method_handlers['fields.insert'];

        addField.apply({}, [{
            label: "Client Age",
            name: "age",
            type: "number"
        }]);

      assert.equal(Fields.find().count(), 1);
    });
  });
}
