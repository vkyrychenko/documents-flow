// All fields-related publications

import { Meteor } from 'meteor/meteor';
import { Fields } from '../fields.js';

Meteor.publish('fields.all', function () {
  return Fields.find();
});
