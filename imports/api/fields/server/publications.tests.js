// Tests for the fields publications

import {assert} from 'chai';
import {Fields} from '../fields.js';
import {PublicationCollector} from 'meteor/johanbrook:publication-collector';
import './publications.js';

describe('fields publications', function () {
    beforeEach(function () {
        Fields.remove({});

        Fields.insert({
            label: "Client Age",
            name: "age",
            type: "number"
        });
    });

    describe('fields.all', function () {
        it('sends all fields', function (done) {
            const collector = new PublicationCollector();
            collector.collect('fields.all', (collections) => {

                assert.equal(collections.fields ? collections.fields.length : 0, 1);
                done();
            });
        });
    });
});
