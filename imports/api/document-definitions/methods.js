import { Meteor } from 'meteor/meteor';
import { DocumentDefinitions } from './document-definitions.js';

Meteor.methods({
    'document-definitions.insert'(definition) {

        const validationContext = DocumentDefinitions.simpleSchema().newContext();
        const isValid = validationContext.isValid();

        console.log(`document-definitions.insert - isValid: ${isValid}`);

        if(!isValid){
            return null;
        }

        return DocumentDefinitions.insert(definition);
    },
});
