import { Meteor } from 'meteor/meteor';
import { assert } from 'chai';
import { DocumentDefinitions } from './document-definitions.js';

if (Meteor.isServer) {
    describe('DocumentDefinitions collection', function () {
        beforeEach(function () {
            console.log('documentdefinitions: clearing collection');
            DocumentDefinitions.remove({});
        });
        it('insert correctly', function () {
            const definitionId = DocumentDefinitions.insert({
                schema: {
                    fields: [
                        {
                            label: "Client Name",
                            name: "name",
                            type: "text",
                            maxLength: 100
                        }
                    ]
                }
            });
            const added = DocumentDefinitions.find({ _id: definitionId });
            const collectionName = added._getCollectionName();
            const count = added.count();

            assert.equal(collectionName, 'documentdefinitions');
            assert.equal(count, 1);
        });
    });
}
