import { Mongo } from 'meteor/mongo';
import { DocumentDefinitionsSchema } from './schemas/DocumentDefinitionsSchema';

const DocumentDefinitions = new Mongo.Collection('documentdefinitions');

DocumentDefinitions.attachSchema(DocumentDefinitionsSchema);

DocumentDefinitions.allow({
    insert(definition){
        return true;
    }
});

export { DocumentDefinitions };
