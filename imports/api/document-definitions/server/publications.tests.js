import {assert} from 'chai';
import {DocumentDefinitions} from '../document-definitions';
import {PublicationCollector} from 'meteor/johanbrook:publication-collector';
import './publications.js';

describe('document-definitions publications', function () {
    beforeEach(function () {
        DocumentDefinitions.remove({});

        DocumentDefinitions.insert({
            schema: {
                fields: [
                    {
                        label: "Client Name",
                        name: "name",
                        type: "text",
                        maxLength: 100
                    }
                ]
            }
        });
    });

    describe('document-definitions.all', function () {
        it('sends all document-definitions', function (done) {
            const collector = new PublicationCollector();
            collector.collect('document-definitions.all', (collections) => {
                assert.equal(collections.documentdefinitions ? collections.documentdefinitions.length : 0, 1);
                done();
            });
        });
    });
});
