import {Meteor} from 'meteor/meteor';
import {DocumentDefinitions} from '../document-definitions';

Meteor.publish('document-definitions.all', function () {
    return DocumentDefinitions.find();
});