import { Meteor } from 'meteor/meteor';
import { assert } from 'chai';
import { DocumentDefinitions } from './document-definitions.js';
import './methods.js';

if (Meteor.isServer) {
  describe('document-definitions methods', function () {
    beforeEach(function () {
        DocumentDefinitions.remove({});
    });

    it('can add a new document-definitions', function () {
      const addDocDefinition = Meteor.server.method_handlers['document-definitions.insert'];

        addDocDefinition.apply({}, [{
            schema: {
                fields: [
                    {
                        label: "Client Name",
                        name: "name",
                        type: "text",
                        maxLength: 100
                    }
                ]
            }
        }]);

      assert.equal(DocumentDefinitions.find().count(), 1);
    });
  });
}
