import SimpleSchema from 'simpl-schema';
import { FieldsSchema } from '../../fields/schemas/FieldsSchema';

const FieldsOptionSchema = new SimpleSchema({
    fields: [FieldsSchema]
});

export const DocumentDefinitionsSchema = new SimpleSchema({
    schema: FieldsOptionSchema
});