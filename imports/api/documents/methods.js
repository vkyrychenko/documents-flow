import { Meteor } from 'meteor/meteor';
import { Documents } from './documents.js';

Meteor.methods({
    'documents.insert'(doc) {

        const validationContext = Documents.simpleSchema(doc).newContext();
        const isValid = validationContext.isValid();

        console.log(`documents.insert - isValid: ${isValid}`);

        if(!isValid){
            return null;
        }
        return Documents.insert(doc);
    },
});
