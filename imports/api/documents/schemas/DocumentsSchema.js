import SimpleSchema from 'simpl-schema';

import {FieldsSchema} from '../../fields/schemas/FieldsSchema';

// Extend FieldsSchema
const DocumentFieldsSchema = new SimpleSchema();
DocumentFieldsSchema.extend(FieldsSchema);
DocumentFieldsSchema.extend({
    value: String
});

const DocumentsSchema = new SimpleSchema({
    name: String,
    label: String,
    fields: [DocumentFieldsSchema],
});

export {DocumentsSchema, DocumentFieldsSchema}