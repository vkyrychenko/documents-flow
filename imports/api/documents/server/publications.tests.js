import {assert} from 'chai';
import {Documents} from '../documents.js';
import {PublicationCollector} from 'meteor/johanbrook:publication-collector';
import './publications.js';

describe('documents publications', function () {
    beforeEach(function () {
        Documents.remove({});

        Documents.insert({
            name: "Test Name",
            label: "Test Label",
            fields: [
                {
                    label: "Client Name",
                    name: "name",
                    type: "text",
                    maxLength: 100,
                    value: "Some data"
                }
            ]
        });
    });

    describe('documents.all', function () {
        it('sends all documents', function (done) {
            const collector = new PublicationCollector();
            collector.collect('documents.all', (collections) => {

                assert.equal(collections.documents ? collections.documents.length : 0, 1);
                done();
            });
        });
    });
});
