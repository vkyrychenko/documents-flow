import { Mongo } from 'meteor/mongo';
import { DocumentsSchema } from './schemas/DocumentsSchema';

const Documents = new Mongo.Collection('documents');

Documents.attachSchema(DocumentsSchema);

Documents.allow({
    insert(doc){
        return true;
    }
});

export { Documents };
