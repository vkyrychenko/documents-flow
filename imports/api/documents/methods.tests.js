import { Meteor } from 'meteor/meteor';
import { assert } from 'chai';
import { Documents } from './documents.js';
import './methods.js';

if (Meteor.isServer) {
  describe('documents methods', function () {
    beforeEach(function () {
        Documents.remove({});
    });

    it('Can add a new document', function () {
      const addDoc = Meteor.server.method_handlers['documents.insert'];

        addDoc.apply({}, [{
            name: "Test Name",
            label: "Test Label",
            fields: [
                {
                    label: "Client Name",
                    name: "name",
                    type: "text",
                    maxLength: 100,
                    value: "Some data"
                }
            ]
        }]);

      assert.equal(Documents.find().count(), 1);
    });
  });
}
