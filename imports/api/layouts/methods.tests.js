import {Meteor} from 'meteor/meteor';
import {assert} from 'chai';
import {Layouts} from './layouts.js';
import './methods.js';
import {Random} from 'meteor/random';

if (Meteor.isServer) {
    describe('Test "layouts" methods', function () {
        beforeEach(function () {
            Layouts.remove({});
        });

        it('Can add a new layout', function () {
            const addLayout = Meteor.server.method_handlers['layouts.insert'];

            addLayout.apply({}, [{
                name: "documentPageLayout",
                label: "Document Page Layout",
                documentDefinitionId: Random.id(),
                header: {
                    rows: [
                        {
                            columns: [
                                {
                                    fieldId: Random.id()
                                },
                                {
                                    fieldId: Random.id()
                                }
                            ]
                        }
                    ]
                },
                buttons: ["Submit"]
            }]);

            assert.equal(Layouts.find().count(), 1);
        });
    });
}
