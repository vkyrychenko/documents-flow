import { Mongo } from 'meteor/mongo';
import { LayoutsSchema } from './schemas/LayoutsSchema';

const Layouts = new Mongo.Collection('layouts');

Layouts.attachSchema(LayoutsSchema);

Layouts.allow({
    insert(layout){
        return true;
    }
});

export { Layouts };
