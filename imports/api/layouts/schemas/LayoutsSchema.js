import SimpleSchema from 'simpl-schema';

const FieldIdsSchema = new SimpleSchema({
    fieldId: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
    }
});

const RowsSchema = new SimpleSchema({
    columns: [FieldIdsSchema]
});

const HeaderSchema = new SimpleSchema({
    rows: [RowsSchema]
});

export const LayoutsSchema = new SimpleSchema({
    name: String,
    label: String,
    documentDefinitionId: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
    },
    header: {
        type: HeaderSchema
    },
    buttons: [String]
});