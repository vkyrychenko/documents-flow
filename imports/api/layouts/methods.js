import { Meteor } from 'meteor/meteor';
import { Layouts } from './layouts.js';

Meteor.methods({
    'layouts.insert'(layout) {

        const validationContext = Layouts.simpleSchema(layout).newContext();
        const isValid = validationContext.isValid();

        console.log(`layouts.insert - isValid: ${isValid}`);

        if(!isValid){
            return null;
        }
        return Layouts.insert(layout);
    },
});
