import {assert} from 'chai';
import {Layouts} from '../layouts.js';
import {PublicationCollector} from 'meteor/johanbrook:publication-collector';
import {Random} from 'meteor/random';
import './publications.js';

describe('Test "layouts" publications', function () {
    beforeEach(function () {
        Layouts.remove({});

        Layouts.insert({
            name: "documentPageLayout",
            label: "Document Page Layout",
            documentDefinitionId: Random.id(),
            header: {
                rows: [
                    {
                        columns: [
                            {
                                fieldId: Random.id()
                            },
                            {
                                fieldId: Random.id()
                            }
                        ]
                    }
                ]
            },
            buttons: ["Submit"]
        });
    });

    describe('layouts.all', function () {
        it('Check if "layouts.all" sends all layouts', function (done) {
            const collector = new PublicationCollector();
            collector.collect('layouts.all', (collections) => {

                assert.equal(collections.layouts ? collections.layouts.length : 0, 1);
                done();
            });
        });
    });
});
