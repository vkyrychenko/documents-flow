import {Meteor} from 'meteor/meteor';
import {Layouts} from '../layouts.js';

Meteor.publish('layouts.all', function () {
    return Layouts.find();
});
