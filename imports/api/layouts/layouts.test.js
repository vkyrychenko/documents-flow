import {Meteor} from 'meteor/meteor';
import {assert} from 'chai';
import {Layouts} from './layouts.js';
import {Random} from 'meteor/random';

if (Meteor.isServer) {
    describe('Testing "layouts" collection', function () {
        beforeEach(function () {
            console.log('layouts: clearing collection');
            Layouts.remove({});
        });
        it('Can insert to layouts collection', function () {
            const layoutId = Layouts.insert({
                name: "documentPageLayout",
                label: "Document Page Layout",
                documentDefinitionId: Random.id(),
                header: {
                    rows: [
                        {
                            columns: [
                                {
                                    fieldId: Random.id()
                                },
                                {
                                    fieldId: Random.id()
                                }
                            ]
                        }
                    ]
                },
                buttons: ["Submit"]
            });
            const added = Layouts.find({_id: layoutId});
            const collectionName = added._getCollectionName();
            const count = added.count();

            assert.equal(collectionName, 'layouts');
            assert.equal(count, 1);
        });
    });
}
