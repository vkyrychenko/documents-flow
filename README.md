### Run tests in console
```bash
npm test
```

### Start application
```bash
npm start
```

### User guide

#### Fields list
Fields predefined in the [fixtures.js](https://bitbucket.org/vkyrychenko/documents-flow/src/7853278468bead54bea8297de4898cacdb157b36/imports/startup/server/fixtures.js?at=master&fileviewer=file-view-default "Source code") for demo purpose.

![fields] (https://bytebucket.org/vkyrychenko/documents-flow/raw/7853278468bead54bea8297de4898cacdb157b36/readme-imgs/fields.png)

#### Document Definitions
According to [Layout documentation](https://docs.bizzstream.com/viewingDocuments/layouts.html):
> *"...a layout is connected to a document definition, the number of different layouts per document definition is infinite"*

That's why layout collection connected to document definitions collection using `documentDefinitionId` field. 
Check [layouts schema](https://bitbucket.org/vkyrychenko/documents-flow/src/7853278468bead54bea8297de4898cacdb157b36/imports/api/layouts/schemas/LayoutsSchema.js?at=master&fileviewer=file-view-default "Source Code") for more details.

By clicking on **"Create document"** button user can create document based on choosen document definition and layout.

![doc-definitions] (https://bytebucket.org/vkyrychenko/documents-flow/raw/7853278468bead54bea8297de4898cacdb157b36/readme-imgs/DocDefinitions.png)

#### Add Document
The page generates using information about document definition and layout. ViewHelper class [[code](https://bitbucket.org/vkyrychenko/documents-flow/src/7853278468bead54bea8297de4898cacdb157b36/imports/api/views/client/views.js?at=master "Source Code")] generate view that is used during rendering page.
View [[code](https://bitbucket.org/vkyrychenko/documents-flow/src/7853278468bead54bea8297de4898cacdb157b36/imports/api/views/client/views.js?at=master)] is a client side collection that loads only in browser "on the fly".
```JavaScript
// file: imports/api/client/views.js
/**
 * Local (client side only) collection to hold views.
 * @type {Mongo.Collection}
 */
const Views = new Mongo.Collection(null);
```

Documents collection store data that was entered by user. Check it's [schema](https://bitbucket.org/vkyrychenko/documents-flow/src/7853278468bead54bea8297de4898cacdb157b36/imports/api/documents/schemas/DocumentsSchema.js?at=master) for details.
After adding document new items will appear in list below the form. Same list of documents can be seen on separate page "Documents".

Saving of the documents starts from catching event in [documents template](https://bitbucket.org/vkyrychenko/documents-flow/src/7853278468bead54bea8297de4898cacdb157b36/imports/ui/pages/documents/documents-create.js?at=master).

```JavaScript
/**
 * DocumentDefinitions: Event Handlers
 * @memberof Client.Templates.DocumentDefinitions
 * @member Events
 */
Template.Documents_create.events({
    'submit .new-document'(event) {
    ...
```

![document-add] (https://bytebucket.org/vkyrychenko/documents-flow/raw/7853278468bead54bea8297de4898cacdb157b36/readme-imgs/Add%20Doc.png)
